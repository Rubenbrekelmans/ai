
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class SpellCorrector {

    final private CorpusReader cr;
    final private ConfusionMatrixReader cmr;

    public SpellCorrector(CorpusReader cr, ConfusionMatrixReader cmr) {
        this.cr = cr;
        this.cmr = cmr;
    }

    private class Tuple implements Comparable<Tuple> {

        String word;
        String prefix;
        String postfix;
        double value;
        boolean canBeFixed = true;
        int index;

        private Tuple(String word, double val, String prefix, String postfix, int index) {
            this.word = word;
            this.value = val;
            this.prefix = prefix;
            this.postfix = postfix;
            this.index = index;
        }

        @Override
        public int compareTo(Tuple o) {
            return value - o.value < 0 ? -1 : 1;
        }
    }

    public String correctPhrase(String phrase) {
        if (phrase == null || phrase.length() == 0) {
            throw new IllegalArgumentException("phrase must be non-empty.");
        }
        if (phrase.equals("at the hame locations there were traces of water")) {
            String[] words = phrase.split(" ");
        }
        String[] words = phrase.split(" ");

        StringBuilder sb = new StringBuilder(phrase.length() + 5);
        /**
         * CODE TO BE ADDED *
         */
        /* this was the code that used a priority queue to adress the words
        List<Tuple> sort = new ArrayList<>();
        
        //calculate probability according to kneserney of the word being correct
        for (int i = 0; i < words.length; i++) {
            if (i > 0 && i < words.length - 1) {
                double val = cr.getSmoothedCount(words[i - 1], words[i], words[i + 1]);
                sort.add(new Tuple(words[i], val, words[i - 1], words[i + 1],i));
            } else if (i == 0) {
                double val = cr.getSmoothedCount("", words[i], words[i + 1]);
                sort.add(new Tuple(words[i], val, "", words[i + 1],i));
            } else {
                double val = cr.getSmoothedCount(words[i - 1], words[i], "");
                sort.add(new Tuple(words[i], val, words[i - 1], "",i));
            }
        }
        //sort the list to have the worst at the beginning
        Collections.sort(sort);

        //change the word plus remove the words to the left and right of it
        for (int i = 0; i < sort.size(); i++) {
            Tuple t = sort.get(i);
            if (t.canBeFixed) {
                String correct = getCorrect(t.word, t.prefix, t.postfix);
                if (!correct.equals(t.word)) {
                    //look up i-1 and i+1 and change boolean.
                    t.word=correct;
                    changeCanBeFixed(sort,t.index);
                }
            }
        }
        //sort it in the order of the sentence
        sort.sort(new Comparator<Tuple>(){
            @Override
            public int compare(Tuple o1, Tuple o2) {
                return o1.index-o2.index;
            }
            
        });
        //create and return the sentence
        for(int i=0;i<sort.size();i++){
            sb.append(sort.get(i).word+" ");
        }
        return sb.toString().trim();
         */
        
        //make sure that the first word is ot corrected if the second word is definitly wrong
        boolean prevCorrect = cr.inVocabulary(words[1]);
        for (int i = 0; i < words.length; i++) {
            String word = words[i];
            String result;
            //we can't change a word if the previous one was corrected or the next one is guaranteed wrong so we skip it
            if(i+1<words.length) prevCorrect = prevCorrect && cr.inVocabulary(words[i+1]);
            if (!prevCorrect ) {
                sb.append(word);
                sb.append(" ");
                prevCorrect = true;
                continue;
            }
            switch (i) {
                case 0:
                    //for an empty prefix
                    result = getCorrect(word, "", i + 1 < words.length ? words[i + 1] : "");
                    break;
                default:
                    //all other cases
                    result = getCorrect(word, words[i - 1], i + 1 < words.length ? words[i + 1] : "");
                    break;
            }

            //we set the variable
            prevCorrect = word.equals(result);
            sb.append(result);
            //add it to the sentence
            sb.append(" ");
        }

        String finalSuggestion = sb.toString();
        return finalSuggestion.trim();

    }

    //used to remove the 2 adjacent words from the priorityqueue
    public void changeCanBeFixed(List<Tuple> list, int i) {
        for (Tuple t : list) {
            if (Math.abs(t.index - i) <= 1) {
                t.canBeFixed = false;
            }
        }
    }

    //mmethod that gets the best word
    public String getCorrect(String word, String prefix, String postfix) {
        Map<String, Double> candidates = getCandidateWords(word);
        double maxValCandidates=0;
        //get the largest value of all the words
        for(Map.Entry<String,Double> m:candidates.entrySet()){
            maxValCandidates=maxValCandidates<m.getValue()?m.getValue():maxValCandidates;
        }
        
        double highest;
        String correct;
        if (cr.inVocabulary(word)) {
            //if its a valid word it should only be compared to the best word 
            //therefor the value is set to maxValCandidates*kneserNey
            highest = maxValCandidates * cr.getKneserNey(prefix, word, postfix);
            correct = word;
        }else{
            //the case that the word is not valid
            highest=Double.MIN_VALUE;
            correct="";
        }

        for (Map.Entry<String, Double> entry : candidates.entrySet()) {
            double confusionCount = entry.getValue();
            double kneserNey = cr.getKneserNey(prefix, entry.getKey(), postfix);
            double result = confusionCount * kneserNey;
            candidates.put(entry.getKey(), result);
            //pick the word with the highest result
            if (result > highest) {
                correct = entry.getKey();
                highest = result;
            }
        }
        //return the word itself
        return correct;

    }

    /**
     * returns a map with candidate words and their noisy channel probability. *
     */
    public Map<String, Double> getCandidateWords(String typo) {
        return new WordGenerator(cr, cmr).getCandidateCorrections(typo);
    }
}
