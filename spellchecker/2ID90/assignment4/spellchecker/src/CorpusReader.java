
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

public class CorpusReader {

    final static String CNTFILE_LOC = "samplecnt.txt";
    final static String VOCFILE_LOC = "samplevoc.txt";

    private HashMap<String, Long> ngrams;
    private Set<String> vocabulary;
    private GoodTuring goodTuring;
    
    public CorpusReader() throws IOException {
        readNGrams();
        readVocabulary();
        goodTuring=new GoodTuring();
    }

    /**
     * Returns the n-gram count of <NGram> in the corpus
     *
     * @param nGram : space-separated list of words, e.g. "adopted by him"
     * @return count of <NGram> in corpus
     */
    public long getNGramCount(String nGram) throws NumberFormatException {
        if (nGram == null || nGram.length() == 0) {
            throw new IllegalArgumentException("NGram must be non-empty.");
        }
        return ngrams.getOrDefault(nGram, 0l);
    }

    private void readNGrams() throws
            FileNotFoundException, IOException, NumberFormatException {
        ngrams = new HashMap<>();

        FileInputStream fis;
        fis = new FileInputStream(CNTFILE_LOC);
        BufferedReader in = new BufferedReader(new InputStreamReader(fis));

        while (in.ready()) {
            String phrase = in.readLine().trim();
            String s1, s2;
            int j = phrase.indexOf(" ");

            s1 = phrase.substring(0, j);
            s2 = phrase.substring(j + 1, phrase.length());

            long count = 0;
            try {
                count = Long.parseLong(s1);
                ngrams.put(s2, count);
            } catch (NumberFormatException nfe) {
                throw new NumberFormatException("NumberformatError: " + s1);
            }
        }
    }

    private void readVocabulary() throws FileNotFoundException, IOException {
        vocabulary = new HashSet<>();

        FileInputStream fis = new FileInputStream(VOCFILE_LOC);
        BufferedReader in = new BufferedReader(new InputStreamReader(fis));

        while (in.ready()) {
            String line = in.readLine();
            vocabulary.add(line);
        }
    }

    /**
     * Returns the size of the number of unique words in the corpus
     *
     * @return the size of the number of unique words in the corpus
     */
    public int getVocabularySize() {
        return vocabulary.size();
    }

    /**
     * Returns the subset of words in set that are in the vocabulary
     *
     * @param set
     * @return
     */
    public HashSet<String> inVocabulary(Set<String> set) {
        HashSet<String> h = new HashSet<>(set);
        h.retainAll(vocabulary);
        return h;
    }

    public boolean inVocabulary(String word) {
        return vocabulary.contains(word);
    }

    /**
     *
     * @param NGram Is a string with spaces containing the prefix!!!!!!!!!!!!!!
     * @return
     */
    public double getSmoothedCount(String prefix,String word,String postfix) {


        double smoothedCount = 0.0;

        /**
         * ADD CODE HERE *
         */   
        //take the average of the 2 bigrams
        double average=goodTuring.getSmoothedCount(prefix+" "+word)+goodTuring.getSmoothedCount(word+" "+postfix);
        average/=2d;
        return average;
        
        
    }
    
    public double getKneserNey(String prefix,String word,String postfix){
        double smoothedCount=0d;
        //try kneser ney on the word i and i-1 but also word i and word i+1
        double temp1=kneserNeySmoothing(prefix,word);
        double temp2=kneserNeySmoothing(word,postfix);
        //if they either return -1 it means that no probability was found because prefix or word was empty
        if(temp1==-1||temp2==-1){
            smoothedCount=Math.max(temp1,temp2);
        }else{
            //multiplied since if a word is not seen ever in the word i and word i+1 combination it probably isn't the correct word
            smoothedCount=temp1*temp2;
        }

        return smoothedCount;
    }
    
    public double delta=0.75;//seemed the best value during testing
    public double countOccurenceBigramLargerZero;
    //http://www.foldl.me/2014/kneser-ney-smoothing/
    public double kneserNeySmoothing(String prefix,String word){
        //handle the special cases
        if(prefix.length()==0){
            return -1;
        }else if(word.length()==0){
            return -1;
        }
        else{
            //apply the formula found in the video lecture
            double firstNumerator=Math.max(getNGramCount(prefix+" "+word)-delta,0);
            double firstDenominator=getNGramCount(prefix);
            //no need to recalculate this for every kneserNeycomputation
            if(countOccurenceBigramLargerZero==0){
                countOccurenceBigramLargerZero=countOccurencesAfterPrefix();
            }
            double secondFraction=countPrefixesPossible(word)/countOccurenceBigramLargerZero;//this is the interpolation factor
            double normalizingConstant=delta/sumCountAfterPrefix(prefix);
            normalizingConstant*=sumOccurenceAfterPrefix(prefix);
            double firstFraction=firstNumerator/firstDenominator;
            double result= (firstFraction+normalizingConstant*secondFraction);
            return result;
        }
    }
        
    //Sum_(w') ( c(w_(i-1),w')
    public double sumCountAfterPrefix(String prefix){
        int amount=0;
        for(String s:vocabulary){
            if(getNGramCount(prefix+" "+s)>0){
                amount+=getNGramCount(prefix+" "+s);
            }
        }
        return amount;
    }

    //{v:C(wi-1,v)>0}|
    public double sumOccurenceAfterPrefix(String prefix){
        int amount=0;
        for(String s:vocabulary){
            if(getNGramCount(prefix+" "+s)>0){
                amount++;
            }
        }
        return amount;
    }   
    
    //|{v:C(v,w)>0}|
    public double countPrefixesPossible(String word){
        double count=0;
        for(String s:vocabulary){
            if(getNGramCount(s+" "+word)>0){
                count++;
            }
        }
       return count;
    }
    
    //Sum w'|{v:C(v,w')>0}|
    public double countOccurencesAfterPrefix(){
        double count=0;
        for(String s:ngrams.keySet()){
            if(s.split(" ").length>1){
                count+=ngrams.get(s);
            }
        }
        return count;
    }
    
    
    public class GoodTuring{
    HashMap<Long,Integer> table= new HashMap<>();
    long highestfreq=-1;
    long lowestfreq=-1;
    long total=0;
        public GoodTuring(){
            //create the frequency table
            Iterator<Entry<String,Long>> it=ngrams.entrySet().iterator();
            while(it.hasNext()){
                Entry<String,Long> val=it.next();
                if(table.containsKey(val.getValue())){
                    int valt=table.get(val.getValue());
                    valt++;
                    highestfreq=table.getOrDefault(highestfreq,-1)<valt?val.getValue():highestfreq;
                    lowestfreq=table.getOrDefault(lowestfreq,1000)>valt?val.getValue():lowestfreq;
                    table.put(val.getValue(),valt);
                    total+=getNGramCount(val.getKey());
                }else{
                    table.put(getNGramCount(val.getKey()),1);
                    highestfreq=table.getOrDefault(highestfreq,-1)<1?val.getValue():highestfreq;
                    lowestfreq=table.getOrDefault(lowestfreq,10000)>1?val.getValue():lowestfreq;
                    total+=getNGramCount(val.getKey());
                }
            }
           
        }
        
       public double getSmoothedCount(String word){
           long NGramCount=getNGramCount(word);
           if(NGramCount==0){
               //get  N1/N for words that are never seen
               return table.get(1l)/(double) total;
           }else{
               //else try to get a count
               //if no count is available for NGramCount+1 if that doesn't exists get the closest one
               int temp1=table.getOrDefault(NGramCount+1,table.get(NGramCount));
              //return (c+1)*C(c+1)/c
              return ((NGramCount+1)*temp1/(double) table.get(NGramCount));              
           }
       }
    
    }
}
