package nl.tue.s2id90.group35;

import static java.lang.Integer.MAX_VALUE;
import static java.lang.Integer.MIN_VALUE;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import nl.tue.s2id90.draughts.DraughtsState;
import nl.tue.s2id90.draughts.player.DraughtsPlayer;
import org10x10.dam.game.Move;

/**
 * Implementation of the DraughtsPlayer interface.
 *
 * @author huub
 */
// ToDo: rename this class (and hence this file) to have a distinct name
//       for your player during the tournament
public class Group35 extends DraughtsPlayer {


    private int bestValue = 0;
    int maxSearchDepth;
    private int myColor;//-1 if white 1 if black;
    Move bestMovePrev = null;
    int multPieces, multAdvance, multCenter, multTriangle;
    String name;
    //Map<DraughtsNode, Integer> map;
    /**
     * boolean that indicates that the GUI asked the player to stop thinking.
     */
    private boolean stopped;

    public Group35() {
        super("images.png"); // ToDo: replace with your own icon
        //these are here since it was used for testing the bot with different values
        this.multPieces = 1000;
        this.multAdvance = 1;
        this.multCenter = 1;
        this.multTriangle = 5;
        this.name = "group35";
       /* map = new TreeMap<DraughtsNode, Integer>(new Comparator<DraughtsNode>() {
            @Override
            public int compare(DraughtsNode o1, DraughtsNode o2) {
                if (o1.getState().isWhiteToMove() == o2.getState().isWhiteToMove()) {
                    return Arrays.equals(o1.getState().getPieces(), o2.getState().getPieces()) ? 0 : 1;
                }
                return 1;
            }

        });*/

    }

    @Override
    public Move getMove(DraughtsState s) {
        myColor = s.isWhiteToMove() ? -1 : 1;
        System.out.println(myColor);
        /*
        Move bestMove = null;
        bestValue = 0;
        DraughtsNode node = new DraughtsNode(s);    // the root of the search tree
        try {
            // compute bestMove and bestValue in a call to alphabeta
            bestValue = alphaBeta(node, MIN_VALUE, MAX_VALUE, maxSearchDepth,maxSearchDepth);

            // store the bestMove found uptill now
            // NB this is not done in case of an AIStoppedException in alphaBeat()
            bestMove = node.getBestMove();

            // print the results for debugging reasons
            System.err.format(
                    "%s: depth= %2d, best move = %5s, value=%d\n",
                    this.getClass().getSimpleName(), maxSearchDepth, bestMove, bestValue
            );
        } catch (AIStoppedException ex) {
            /* nothing to do         }

        if (bestMove == null) {
            System.err.println("no valid move found!");
            return getRandomValidMove(s);
        } else {
            return bestMove;
        }*/
        return iterativeDeepening(s);
    }

    /**
     * This method's return value is displayed in the AICompetition GUI.
     *
     * @return the value for the draughts state s as it is computed in a call to
     * getMove(s).
     */
    @Override
    public Integer getValue() {
        return bestValue;
    }

    /**
     * Tries to make alphabeta search stop. Search should be implemented such
     * that it throws an AIStoppedException when boolean stopped is set to true;
     *
     */
    @Override
    public void stop() {
        stopped = true;
    }

    public Move iterativeDeepening(DraughtsState s) {

        Move bestMove = null;
        bestValue = 0;
        DraughtsNode node = new DraughtsNode(s);    // the root of the search tree
        int startingSearchDepth = 3;
        int depth = startingSearchDepth;

        try {
            while (!stopped) {
                // compute bestMove and bestValue in a call to alphabeta
                bestValue = alphaBeta(node, MIN_VALUE, MAX_VALUE, depth, true);

                // store the bestMove found uptill now
                // NB this is not done in case of an AIStoppedException in alphaBeat()
                bestMove = node.getBestMove();
                bestMovePrev = bestMove;
                // print the results for debugging reasons
                System.err.format(
                        "%s: depth= %2d, best move = %5s, value=%d \n",
                        this.getClass().getSimpleName(), depth, bestMove, bestValue
                );
                //map.clear();
                depth++;
            }
        } catch (AIStoppedException ex) {

        }
        if (bestMove == null) {
            System.err.println("no valid move found!");
            return getRandomValidMove(s);
        } else {
            return bestMove;
        }
    }

    /**
     * returns random valid move in state s, or null if no moves exist.
     */
    Move getRandomValidMove(DraughtsState s) {
        List<Move> moves = s.getMoves();
        Collections.shuffle(moves);
        Iterator<Move> it = moves.iterator();
        while (it.hasNext()) {
            Move m = it.next();
            boolean x = m.isBlackMove();
            boolean y = !s.isWhiteToMove();
            if (x && y || !x && !y) {
                return m;
            }
        }
        return null;
    }

    /**
     * Implementation of alphabeta that automatically chooses the white player
     * as maximizing player and the black player as minimizing player.
     *
     * @param node contains DraughtsState and has field to which the best move
     * can be assigned.
     * @param alpha
     * @param beta
     * @param depth maximum recursion Depth
     * @return the computed value of this node
     * @throws AIStoppedException
     *
     */
    int alphaBeta(DraughtsNode node, int alpha, int beta, int depth, boolean setMove)
            throws AIStoppedException {
        if ((node.getState().isWhiteToMove() && myColor == -1) || (!node.getState().isWhiteToMove() && myColor == 1)) {
            return alphaBetaMax(node, alpha, beta, depth, setMove);
        } else {
            return alphaBetaMin(node, alpha, beta, depth, setMove);
        }
    }

    /**
     * Does an alphabeta computation with the given alpha and beta where the
     * player that is to move in node is the minimizing player.
     *
     * <p>
     * Typical pieces of code used in this method are:
     * <ul> <li><code>DraughtsState state = node.getState()</code>.</li>
     * <li><code> state.doMove(move); .... ; state.undoMove(move);</code></li>
     * <li><code>node.setBestMove(bestMove);</code></li>
     * <li><code>if(stopped) { stopped=false; throw new AIStoppedException(); }</code></li>
     * </ul>
     * </p>
     *
     * @param node contains DraughtsState and has field to which the best move
     * can be assigned.
     * @param alpha
     * @param beta
     * @param depth maximum recursion Depth
     * @return the compute value of this node
     * @throws AIStoppedException thrown whenever the boolean stopped has been
     * set to true.
     */
    //black players turn
    int alphaBetaMin(DraughtsNode node, int alpha, int beta, int depth, boolean setMove)
            throws AIStoppedException {
        DraughtsState state = node.getState();
        if (stopped) {
            stopped = false;
            throw new AIStoppedException();
        }
        //prune if there are no more valid moves
        if (depth == 0 || state.isEndState()) {
            return evaluate(node.getState());
        }

        // ToDo: write an alphabeta search to compute bestMove and value
        List<Move> validMoves = state.getMoves();
        int lowestValue = Integer.MAX_VALUE;
        Move bestMove = null;
        if (validMoves.isEmpty()) {
            lowestValue = evaluate(node.getState());
        }
//        if (map.containsKey(new DraughtsNode(node.getState()))) {
//            return map.get(new DraughtsNode(node.getState()));
//        }
        List<Move> forcedMoves = validMoves.stream()
                .filter(m -> m.isCapture())
                .collect(Collectors.toList());

        for (Move m : forcedMoves.size() > 0 ? forcedMoves : validMoves) {

            state.doMove(m);
            int value;

            value = alphaBeta(node, alpha, beta, depth - 1, false);
            beta = Math.min(beta, value);
            if (value < lowestValue) {
                lowestValue = value;
                bestMove = m;
            }
            if (beta <= alpha) {
                state.undoMove(m);
                node.setBestMove(m);
                // map.put(new DraughtsNode(state), lowestValue);
                return value;
            }

            state.undoMove(m);
        }

        //map.put(new DraughtsNode(state), lowestValue);
        node.setBestMove(bestMove);
        return lowestValue;
    }

    //white players turn
    int alphaBetaMax(DraughtsNode node, int alpha, int beta, int depth, boolean setMove)
            throws AIStoppedException {
        DraughtsState state = node.getState();
        if (stopped) {
            stopped = false;
            throw new AIStoppedException();
        }
        if (depth == 0 || state.isEndState()) {
            return evaluate(node.getState());
        }

        // ToDo: write an alphabeta search to compute bestMove and value
        List<Move> validMoves = state.getMoves();
        int highestValue = Integer.MIN_VALUE;
        Move bestMove = null;

//        if (map.containsKey(new DraughtsNode(node.getState()))) {
//            System.out.print("contained ");
//            return map.get(new DraughtsNode(node.getState()));
//        }
        List<Move> forcedMoves = validMoves.stream()
                .filter(m -> m.isCapture())
                .collect(Collectors.toList());

        for (Move m : forcedMoves.size() > 0 ? forcedMoves : validMoves) {

            state.doMove(m);
            int value;

            value = alphaBeta(node, alpha, beta, depth - 1, false);

            alpha = Math.max(alpha, value);

            if (value > highestValue) {
                highestValue = value;
                bestMove = m;
            }

            if (alpha >= beta) {
                state.undoMove(m);
                node.setBestMove(m);
                // map.put(new DraughtsNode(state), highestValue);
                return value;
            }

            state.undoMove(m);
        }

        // map.put(new DraughtsNode(state), highestValue);
        node.setBestMove(bestMove);
        return highestValue;
    }

    /**
     * A method that evaluates the given state.
     */
    // ToDo: write an appropriate evaluation function
    //color 1 if black -1 if white
    int evaluate(DraughtsState state) {
        int[] board = (int[]) state.getPieces().clone();
        int color = myColor;
        double advance = evaluateAdvance(board, color);
        double pieces = evaluatePieces(board, color);
        double center = evaluateCenter(board, color);
        double triangle = evaluateTriangle(board, color);

        double finalScore = multPieces * pieces + multAdvance * advance + multCenter * center + multTriangle * triangle;

        return (int) finalScore;
    }

    //checks for the advancement on the board
    //color 1 if black -1 if white
    double evaluateAdvance(int[] board, int color) {
        int whiteScore = 0;
        int blackScore = 0;

        for (int i = 0; i < board.length; i++) {
            if (board[i] != 0) {
                int multiplier = 1;
                if (board[i] % 2 == 0) {//black piece
                    if (board[i] % 4 == 0) {
                        multiplier = 3;
                    }
                    blackScore += multiplier * Math.ceil(i / 5.0);
                } else {//white piece
                    if (board[i] % 3 == 0) {
                        multiplier = 3;
                    }
                    whiteScore += multiplier *  Math.floor(i / 5.0);
                }
            }
        }

        if (color == -1) {
            return whiteScore;

        } else {
            return blackScore;

        }
    }

    double evaluatePieces(int[] board, int color) {
        int whiteScore = 0;
        int blackScore = 0;
        for (int i = 0; i < board.length; i++) {
            if (board[i] != 0) {
                int multiplier = 1;
                if (board[i] % 2 == 0) {//black piece
                    if (board[i] % 4 == 0) {
                        multiplier = 4;
                    }
                    blackScore += multiplier * 1;
                } else {//white piece
                    if (board[i] % 3 == 0) {
                        multiplier = 4;
                    }
                    whiteScore += multiplier * 1;
                }
            }
        }
        int returnValue;
        if (color == -1) {
            returnValue = whiteScore - blackScore;

        } else {
            returnValue = blackScore - whiteScore;

        }

        return returnValue;
    }

    //color 1 if black -1 if white
    double evaluateCenter(int[] board, int color) {
        int whiteScore = 0;
        int blackScore = 0;
        for (int i = 0; i < board.length; i++) {
            if (board[i] != 0) {
                int multiplier = 1;
                if (board[i] % 2 == 0) {//black piece
                    if (board[i] % 4 == 0) {
                        multiplier = 3;
                    }
                    blackScore += multiplier * (6 - scoring[i]);
                } else {//white piece
                    if (board[i] % 3 == 0) {
                        multiplier = 3;
                    }
                    whiteScore += multiplier * (6 - scoring[i]);
                }
            }
        }
        if (color == -1) {
            return whiteScore;

        } else {
            return blackScore;

        }
    }

    double evaluateTriangle(int[] board, int color) {
        int whiteScore = 0;
        int blackScore = 0;
        for (int i = 0; i < board.length; i++) {
            if (board[i] != 0) {
                if (board[i] % 2 == 0) {//black piece
                    if (i >= 6) {
                        if (board[i - 5] % 2 == 0) {//is there a black piece right-behind?
                            blackScore += 1;
                        }
                    }
                    if (i >= 7) {
                        if (board[i - 6] % 2 == 0) {//is there a black piece left-behind?
                            blackScore += 1;
                        }
                    }
                } else {//white piece
                    if (i <= 45) {
                        if (board[i + 5] == 1 || board[i + 5] == 3) {//is there a black piece left-behind?
                            whiteScore += 1;
                        }
                    }
                    if (i <= 44) {
                        if (board[i + 6] == 1 || board[i + 6] == 3) {//is there a black piece left-behind?
                            whiteScore += 1;
                        }
                    }
                }
            }
        }
        if (color == -1) {
            return  whiteScore - blackScore;

        } else {
            return blackScore - whiteScore;

        }
    }

    final static int[] scoring = {
        0, 5, 5, 5, 5, 5,//r1
        5, 4, 4, 4, 4,//r2
        4, 3, 3, 3, 5,//r3
        5, 3, 2, 2, 4,//r4
        4, 4, 1, 3, 5,//r5
        5, 3, 1, 2, 4,//r6
        4, 2, 2, 3, 5,//r7
        5, 3, 3, 3, 4,//r8
        4, 4, 4, 4, 5,//r9
        5, 5, 5, 5, 5 //r10
    };

}
